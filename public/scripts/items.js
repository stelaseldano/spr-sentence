var loadTrials = function() {
	var groups = [[], [], [], [], []];
	var lists = [[], [], [], [], [], []];

	var items = [
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][0]['v_name'] + " hat letzte Woche Pinguine gefüttert.",
			"falsifying": exp.names['wieder'][0]['f_name'] + " hat noch nie Pinguine gefüttert.",
			"neutral": "Fritz hat noch nie Pinguine gefüttert.",
			"test-v": "Heute hat " + exp.names['wieder'][0]['v_name'] + " wieder Pinguine gefüttert und freut sich.",
			"test-f": "Heute hat " + exp.names['wieder'][0]['f_name'] + " wieder Pinguine gefüttert und freut sich.",
			"test-n": "Heute hat " + exp.names['wieder'][0]['n_name'] + " wieder Pinguine gefüttert und freut sich."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][1]['v_name'] + " hat bereits rote Handschuhe gekauft.",
			"falsifying": exp.names['wieder'][1]['f_name'] + " hat bisher nie rote Handschuhe gekauft.",
			"neutral": "Jonas hat bisher nie rote Handschuhe gekauft.",
			"test-v": "Heute hat " + exp.names['wieder'][1]['v_name'] + " wieder rote Handschuhe gekauft und sie gleich angezogen.",
			"test-f": "Heute hat " + exp.names['wieder'][1]['f_name'] + " wieder rote Handschuhe gekauft und sie gleich angezogen.",
			"test-n": "Heute hat " + exp.names['wieder'][1]['n_name'] + " wieder rote Handschuhe gekauft und sie gleich angezogen."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][2]['v_name'] + " hat letztes Jahr an einem Marathon teilgenommen.",
			"falsifying": exp.names['wieder'][2]['f_name'] + " hat niemals an einem Marathon teilgenommen.",
			"neutral": "Karl hat niemals an einem Marathon teilgenommen.",
			"test-v": "Dieses Jahr hat " + exp.names['wieder'][2]['v_name'] + " wieder an einem Marathon teilgenommen und ist stolz.",
			"test-f": "Dieses Jahr hat " + exp.names['wieder'][2]['f_name'] + "  wieder an einem Marathon teilgenommen und ist stolz.",
			"test-n": "Dieses Jahr hat " + exp.names['wieder'][2]['n_name'] + " wieder an einem Marathon teilgenommen und ist stolz."
		},
		{
			"trigger": "wieder",         
			"verifying": exp.names['wieder'][3]['v_name'] + " ist oft eine Auszeichnung als bester Mitarbeiter verliehen worden.",
			"falsifying": exp.names['wieder'][3]['f_name'] + " ist bis dato eine Auszeichnung als bester Mitarbeiter immer entgangen.",
			"neutral": "Jan ist oft eine Auszeichnung als bester Mitarbeiter verliehen worden.",
			"test-v": "Letzten Montag ist " + exp.names['wieder'][3]['v_name'] + " wieder als bester Mitarbeiter ausgezeichnet worden und das freut ihn.",
			"test-f": "Letzten Montag ist " + exp.names['wieder'][3]['f_name'] + " wieder als bester Mitarbeiter ausgezeichnet worden und das freut ihn.",
			"test-n": "Letzten Montag ist " + exp.names['wieder'][3]['n_name'] + " wieder als bester Mitarbeiter ausgezeichnet worden und das freut ihn."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][4]['v_name'] + " hat jedes Angebot für eine Lebensversicherung abgelehnt.",
			"falsifying": exp.names['wieder'][4]['f_name'] + " hat vor zehn Jahren ein Angebot für eine Lebensversicherung angenommen.",
			"neutral": "Fritz hat jedes Angebot für eine Lebensversicherung abgelehnt.",
			"test-v": "Gestern hat " + exp.names['wieder'][4]['v_name'] + " wieder ein Angebot für eine Lebensversicherung abgelehnt, das ihm unterbreitet worden ist.",
			"test-f": "Gestern hat " + exp.names['wieder'][4]['f_name'] + " wieder ein Angebot für eine Lebensversicherung abgelehnt, das ihm unterbreitet worden ist.",
			"test-n": "Gestern hat " + exp.names['wieder'][4]['n_name'] + " wieder ein Angebot für eine Lebensversicherung abgelehnt, das ihm unterbreitet worden ist."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][5]['v_name'] + " hat letztes Jahr Tinas Charme nachgegeben.",
			"falsifying": exp.names['wieder'][5]['f_name'] + " hat Tinas Charme immer widerstanden.",
			"neutral": "Karl hat Inges Charme immer widerstanden.",
			"test-v": "Letzte Woche hat " + exp.names['wieder'][5]['v_name'] + " wieder Tinas Charme nachgegeben und er erzählt es Paul.",
			"test-f": "Letzte Woche hat " + exp.names['wieder'][5]['f_name'] + " wieder Tinas Charme nachgegeben und er erzählt es Paul.",
			"test-n": "Letzte Woche hat " + exp.names['wieder'][5]['n_name'] + " wieder Tinas Charme nachgegeben und er erzählt es Paul."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][6]['v_name'] + " hat vor zwei Jahren Susannes Geburtstag vergessen.",
			"falsifying": exp.names['wieder'][6]['f_name'] + " hat stets an Susannes Geburtstag gedacht.",
			"neutral": "Karl hat vor zwei Jahren Susannes Geburtstag vergessen.",
			"test-v": "Dieses Jahr hat " + exp.names['wieder'][6]['v_name'] + " wieder Susannes Geburtstag vergessen und er bittet um Entschuldigung.",
			"test-f": "Dieses Jahr hat " + exp.names['wieder'][6]['f_name'] + " wieder Susannes Geburtstag vergessen und er bittet um Entschuldigung.",
			"test-n": "Dieses Jahr hat " + exp.names['wieder'][6]['n_name'] + " wieder Susannes Geburtstag vergessen und er bittet um Entschuldigung."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][7]['v_name'] + " hat sich letztes Jahr einen Hund gekauft.",
			"falsifying": exp.names['wieder'][7]['f_name'] + " hat sich in seinem ganzen Leben noch keinen Hund gekauft.",
			"neutral": "Anselm hat sich in seinem ganzen Leben noch keinen Hund gekauft.",
			"test-v": "Gestern hat " + exp.names['wieder'][7]['v_name'] + " sich wieder einen Hund gekauft und er nennt ihn Rex.",
			"test-f": "Gestern hat " + exp.names['wieder'][7]['f_name'] + " sich wieder einen Hund gekauft und er nennt ihn Rex.",
			"test-n": "Gestern hat " + exp.names['wieder'][7]['n_name'] + " sich wieder einen Hund gekauft und er nennt ihn Rex."
		},
		{	"trigger": "wieder",
			"verifying": exp.names['wieder'][8]['v_name'] + " hat des Öfteren eine Rede vor mehr als hundert Menschen gehalten.",
			"falsifying": exp.names['wieder'][8]['f_name'] + " hat bisher nur Reden vor weniger als hundert Menschen gehalten.",
			"neutral": "Dirk hat des Öfteren eine Rede vor mehr als hundert Menschen gehalten.",
			"test-v": "Letzten Samstag hat " + exp.names['wieder'][8]['v_name'] + " wieder eine Rede vor mehr als hundert Menschen gehalten und hat viel Applaus bekommen.",
			"test-f": "Letzten Samstag hat " + exp.names['wieder'][8]['f_name'] + " wieder eine Rede vor mehr als hundert Menschen gehalten und hat viel Applaus bekommen.",
			"test-n": "Letzten Samstag hat " + exp.names['wieder'][8]['n_name'] + " wieder eine Rede vor mehr als hundert Menschen gehalten und hat viel Applaus bekommen."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][9]['v_name'] + " hat in seinem Leben viele Blind Dates gehabt.",
			"falsifying": exp.names['wieder'][9]['f_name'] + " hat bis zum heutigen Tag Blind Dates gemieden.",
			"neutral": "Fritz hat bis zum heutigen Tag Blind Dates gemieden.",
			"test-v": "Heute hat " + exp.names['wieder'][9]['v_name'] + " wieder ein Blind Date gehabt und es war enttäuschend.",
			"test-f": "Heute hat " + exp.names['wieder'][9]['f_name'] + " wieder ein Blind Date gehabt und es war enttäuschend.",
			"test-n": "Heute hat " + exp.names['wieder'][9]['n_name'] + " wieder ein Blind Date gehabt und es war enttäuschend."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][10]['v_name'] + " ist letztes Jahr ins Ausland gereist. ",
			"falsifying": exp.names['wieder'][10]['f_name'] + " hat bis jetzt auf Reisen ins Ausland verzichtet.",
			"neutral": "Jörg hat bis jetzt auf Reisen ins Ausland verzichtet.",
			"test-v": "Dieses Jahr ist " + exp.names['wieder'][10]['v_name'] + " wieder ins Ausland gereist und hat Urlaub in Brasilien gemacht.",
			"test-f": "Dieses Jahr ist " + exp.names['wieder'][10]['f_name'] + " wieder ins Ausland gereist und hat Urlaub in Brasilien gemacht.",
			"test-n": "Dieses Jahr ist " + exp.names['wieder'][10]['n_name'] + " wieder ins Ausland gereist und hat Urlaub in Brasilien gemacht."
		},
		{
			"trigger": "wieder",
			"verifying": exp.names['wieder'][11]['v_name'] + " hat häufiger eine Kostümparty veranstaltet.",
			"falsifying": exp.names['wieder'][11]['f_name'] + " hat in der Vergangenheit nie eine Kostümparty veranstaltet.",
			"neutral": "Jörg hat in der Vergangenheit nie eine Kostümparty veranstaltet.",
			"test-v": "Letzte Woche hat " + exp.names['wieder'][11]['v_name'] + " wieder eine Kostümparty veranstaltet und ist als Käfer gegangen.",
			"test-f": "Letzte Woche hat " + exp.names['wieder'][11]['f_name'] + " wieder eine Kostümparty veranstaltet und ist als Käfer gegangen.",
			"test-n": "Letzte Woche hat Karl wieder eine Kostümparty veranstaltet und ist als Käfer gegangen."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][0]['v_name'] + " ist in Inge verliebt.",
			"falsifying": exp.names['wissen'][0]['f_name'] + " ist nicht in Inge verliebt.",
			"neutral": "Anselm ist nicht in Inge verliebt.",
			"test-v": "Inge weiß, dass " + exp.names['wissen'][0]['v_name'] + " in sie verliebt ist und freut sich.",
			"test-f": "Inge weiß, dass " + exp.names['wissen'][0]['f_name'] + " in sie verliebt ist und freut sich.",
			"test-n": "Inge weiß, dass " + exp.names['wissen'][0]['n_name'] + " in sie verliebt ist und freut sich."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][1]['v_name'] + " hat eine Katze.",
			"falsifying": exp.names['wissen'][1]['f_name'] + " hat keine Katze.",
			"neutral": "Moritz hat einen Hund.",
			"test-v": "Inge weiß, dass " + exp.names['wissen'][1]['v_name'] + " eine Katze hat und findet das gut.",
			"test-f": "Inge weiß, dass " + exp.names['wissen'][1]['f_name'] + " eine Katze hat und findet das gut.",
			"test-n": "Inge weiß, dass " + exp.names['wissen'][1]['n_name'] + " eine Katze hat und findet das gut."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][2]['v_name'] + " hat einen Bruder.",
			"falsifying": exp.names['wissen'][2]['f_name'] + " hat keinen Bruder.",
			"neutral": "Moritz hat eine Schwester.",
			"test-v": "Vera weiß, dass " + exp.names['wissen'][2]['v_name'] + " einen Bruder hat und erzählt das Susanne.",
			"test-f": "Vera weiß, dass " + exp.names['wissen'][2]['f_name'] + " einen Bruder hat und erzählt das Susanne.",
			"test-n": "Vera weiß, dass " + exp.names['wissen'][2]['n_name'] + "einen Bruder hat und erzählt das Susanne."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][3]['v_name'] + " ist krank.",
			"falsifying": exp.names['wissen'][3]['f_name'] + " ist nicht krank.",
			"neutral": "Markus ist nicht krank.",
			"test-v": "Tina weiß, dass " + exp.names['wissen'][3]['v_name'] + " krank ist und macht sich Sorgen.",
			"test-f": "Tina weiß, dass " + exp.names['wissen'][3]['f_name'] + " krank ist und macht sich Sorgen.",
			"test-n": "Tina weiß, dass " + exp.names['wissen'][3]['n_name'] + " krank ist und macht sich Sorgen."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][4]['v_name'] + " kann mit den Ohren wackeln.",
			"falsifying": exp.names['wissen'][4]['f_name'] + " kann nicht mit den Ohren wackeln.",
			"neutral": "Karl kann mit den Ohren wackeln.",
			"test-v": "Anna weiß, dass " + exp.names['wissen'][4]['v_name'] + " mit den Ohren wackeln kann und findet das erstaunlich.",
			"test-f": "Anna weiß, dass " + exp.names['wissen'][4]['f_name'] + " mit den Ohren wackeln kann und findet das erstaunlich.",
			"test-n": "Anna weiß, dass " + exp.names['wissen'][4]['n_name'] + " mit den Ohren wackeln kann und findet das erstaunlich."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][5]['v_name'] + " hat eine Liebhaberin.",
			"falsifying": exp.names['wissen'][5]['f_name'] + " hat keine Liebhaberin.",
			"neutral": "Karl hat eine Liebhaberin.",
			"test-v": "Inge weiß, dass " + exp.names['wissen'][5]['v_name'] + " eine Liebhaberin hat und behält das für sich.",
			"test-f": "Inge weiß, dass " + exp.names['wissen'][5]['f_name'] + " eine Liebhaberin hat und behält das für sich.",
			"test-n": "Inge weiß, dass " + exp.names['wissen'][5]['n_name'] + " eine Liebhaberin hat und behält das für sich."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][6]['v_name'] + " hat eine Tätowierung.",
			"falsifying": exp.names['wissen'][6]['f_name'] + " hat keine Tätowierung.",
			"neutral": "Dominic hat keine Tätowierung.",
			"test-v": "Inge weiß, dass " + exp.names['wissen'][6]['v_name'] + " eine Tätowierung hat und findet so etwas schön.",
			"test-f": "Inge weiß, dass " + exp.names['wissen'][6]['f_name'] + " eine Tätowierung hat und findet so etwas schön.",
			"test-n": "Inge weiß, dass " + exp.names['wissen'][6]['n_name'] + " eine Tätowierung hat und findet so etwas schön."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][7]['v_name'] + " kann gut tanzen.",
			"falsifying": exp.names['wissen'][7]['f_name'] + " kann nicht gut tanzen.",
			"neutral": "Karl kann gut tanzen.",
			"test-v": "Vera weiß, dass " + exp.names['wissen'][7]['v_name'] + " gut tanzen kann und will mit ihm tanzen.",
			"test-f": "Vera weiß, dass " + exp.names['wissen'][7]['f_name'] + " gut tanzen kann und will mit ihm tanzen.",
			"test-n": "Vera weiß, dass " + exp.names['wissen'][7]['n_name'] + " gut tanzen kann und will mit ihm tanzen."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][8]['v_name'] + " hat eine Stiefmutter.",
			"falsifying": exp.names['wissen'][8]['f_name'] + " hat keine Stiefmutter.",
			"neutral": "Hans hat eine Stiefmutter.",
			"test-v": "Tina weiß, dass " + exp.names['wissen'][8]['v_name'] + " eine Stiefmutter hat und erzählt es Fritz.",
			"test-f": "Tina weiß, dass " + exp.names['wissen'][8]['f_name'] + " eine Stiefmutter hat und erzählt es Fritz.",
			"test-n": "Tina weiß, dass " + exp.names['wissen'][8]['n_name'] + " eine Stiefmutter hat und erzählt es Fritz."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][9]['v_name'] + " hat ein Auto.",
			"falsifying": exp.names['wissen'][9]['f_name'] + " hat kein Auto.",
			"neutral": "Karl hat ein Auto.",
			"test-v": "Susanne weiß, dass " + exp.names['wissen'][9]['v_name'] + " ein Auto hat und bittet ihn sie mitzunehmen.",
			"test-f": "Susanne weiß, dass " + exp.names['wissen'][9]['f_name'] + " ein Auto hat und bittet ihn sie mitzunehmen.",
			"test-n": "Susanne weiß, dass " + exp.names['wissen'][9]['n_name'] + " ein Auto hat und bittet ihn sie mitzunehmen."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][10]['v_name'] + " hat Depressionen.",
			"falsifying": exp.names['wissen'][10]['f_name'] + " hat keine Depressionen.",
			"neutral": "Fritz hat Depressionen.",
			"test-v": "Nadine weiß, dass " + exp.names['wissen'][10]['v_name'] + " Depressionen hat und empfiehlt ihm einen Arzt.",
			"test-f": "Nadine weiß, dass " + exp.names['wissen'][10]['f_name'] + " Depressionen hat und empfiehlt ihm einen Arzt.",
			"test-n": "Nadine weiß, dass " + exp.names['wissen'][10]['n_name'] + " Depressionen hat und empfiehlt ihm einen Arzt."
		},
		{
			"trigger": "wissen",
			"verifying": exp.names['wissen'][11]['v_name'] + " ist geschieden.",
			"falsifying": exp.names['wissen'][11]['f_name'] + " ist nicht geschieden.",
			"neutral": "Anselm ist nicht geschieden.",
			"test-v": "Nadine weiß, dass " + exp.names['wissen'][11]['v_name'] + " geschieden ist und stellt ihm eine Kollegin vor.",
			"test-f": "Nadine weiß, dass " + exp.names['wissen'][11]['f_name'] + " geschieden ist und stellt ihm eine Kollegin vor.",
			"test-n": "Nadine weiß, dass " + exp.names['wissen'][11]['n_name'] + " geschieden ist und stellt ihm eine Kollegin vor."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][0]['v_name'] + " hat ein Buch geschrieben.",
			"falsifying": exp.names['sein'][0]['f_name'] + " hat kein Buch geschrieben.",
			"neutral": exp.names['sein'][0]['n_name'] + " hat ein Gedicht geschrieben.",
			"test-v": "Maria kauft sich sein Buch und liest es.",
			"test-f": "Maria kauft sich sein Buch und liest es.",
			"test-n": "Maria kauft sich sein Buch und liest es."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][1]['v_name'] + " hat eine Distel.",
			"falsifying": exp.names['sein'][1]['f_name'] + " hat keine Distel.",
			"neutral": exp.names['sein'][1]['n_name'] + " hat einen Dackel.",
			"test-v": "Steffi sticht sich an seiner Distel und flucht.",
			"test-f": "Steffi sticht sich an seiner Distel und flucht.",
			"test-n": "Steffi sticht sich an seiner Distel und flucht."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][2]['v_name'] + " besitzt ein Restaurant.",
			"falsifying": exp.names['sein'][2]['f_name'] + " besitzt kein Restaurant.",
			"neutral": exp.names['sein'][2]['n_name'] + " besitzt einen Friseursalon.",
			"test-v": "Susanne geht in sein Restaurant und trifft Bekannte.",
			"test-f": "Susanne geht in sein Restaurant und trifft Bekannte.",
			"test-n": "Susanne geht in sein Restaurant und trifft Bekannte."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][3]['v_name'] + " hat eine Vespa.",
			"falsifying": exp.names['sein'][3]['n_name'] + " hat keine Vespa.",
			"neutral": exp.names['sein'][3]['n_name'] + " hat einen Volvo.",
			"test-v": "Tina sieht seine Vespa und bewundert sie.",
			"test-f": "Tina sieht seine Vespa und bewundert sie.",
			"test-n": "Tina sieht seine Vespa und bewundert sie."
		},
		{	
			"trigger": "sein",
			"verifying": exp.names['sein'][4]['v_name'] + " besitzt eine Gärtnerei.",
			"falsifying": exp.names['sein'][4]['f_name'] + " besitzt keine Gärtnerei.",
			"neutral": exp.names['sein'][4]['n_name'] + " besitzt einen Bauernhof.",
			"test-v": "Laura besucht ihn in seiner Gärtnerei und geht mit ihm Mittagessen.",
			"test-f": "Laura besucht ihn in seiner Gärtnerei und geht mit ihm Mittagessen.",
			"test-n": "Laura besucht ihn in seiner Gärtnerei und geht mit ihm Mittagessen."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][5]['v_name'] + " hat eine Katze.",
			"falsifying": exp.names['sein'][5]['f_name'] + " hat keine Katze.",
			"neutral": exp.names['sein'][5]['n_name'] + " hat eine kleine Schwester.",
			"test-v": "Susanne streichelt seine Katze und findet sie süß.",
			"test-f": "Susanne streichelt seine Katze und findet sie süß.",
			"test-n": "Susanne streichelt seine Katze und findet sie süß."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][6]['v_name'] + " besitzt ein Taxi.",
			"falsifying": exp.names['sein'][6]['f_name'] + " besitzt kein Taxi.",
			"neutral": exp.names['sein'][6]['n_name'] + " besitzt ein Fahrrad.",
			"test-v": "Anna leiht sich sein Taxi und fährt nach Potsdam.",
			"test-f": "Anna leiht sich sein Taxi und fährt nach Potsdam.",
			"test-n": "Anna leiht sich sein Taxi und fährt nach Potsdam."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][7]['v_name'] + " hat einen Hund.",
			"falsifying": exp.names['sein'][7]['f_name'] + " hat keinen Hund.",
			"neutral": exp.names['sein'][7]['n_name'] + " hat einen Wellensittich.",
			"test-v": "Inge wäscht seinen Hund und füttert ihn.",
			"test-f": "Inge wäscht seinen Hund und füttert ihn.",
			"test-n": "Inge wäscht seinen Hund und füttert ihn."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][8]['v_name'] + " hat einen Fernseher.",
			"falsifying": exp.names['sein'][8]['f_name'] + " hat keinen Fernseher.",
			"neutral": exp.names['sein'][8]['n_name'] + " hat ein Radios.",
			"test-v": "Susanne repariert seinen Fernseher und er ist ihr dankbar.",
			"test-f": "Susanne repariert seinen Fernseher und er ist ihr dankbar.",
			"test-n": "Susanne repariert seinen Fernseher und er ist ihr dankbar."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][9]['v_name'] + " hat zwei Orchideen.",
			"falsifying": exp.names['sein'][9]['f_name'] + " hat keine Orchideen.",
			"neutral": exp.names['sein'][9]['n_name'] + " hat einen Apfelbaum.",
			"test-v": "Tina gießt seine Orchideen und düngt sie.",
			"test-f": "Tina gießt seine Orchideen und düngt sie.",
			"test-n": "Tina gießt seine Orchideen und düngt sie."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][10]['v_name'] + " hat einen Laptop.",
			"falsifying": exp.names['sein'][10]['f_name'] + " hat keinen Laptop.",
			"neutral": exp.names['sein'][10]['n_name'] + " hat ein Mobiltelefon.",
			"test-v": "Inge leiht seinen Laptop aus und bedankt sich mit einem Essen.",
			"test-f": "Inge leiht seinen Laptop aus und bedankt sich mit einem Essen.",
			"test-n": "Inge leiht seinen Laptop aus und bedankt sich mit einem Essen."
		},
		{
			"trigger": "sein",
			"verifying": exp.names['sein'][11]['v_name'] + " trägt eine Brille.",
			"falsifying": exp.names['sein'][11]['f_name'] + " trägt keine Brille.",
			"neutral": exp.names['sein'][11]['n_name'] + " trägt keinen Hut.",
			"test-v": "Susanne bewundert seine Brille und macht ihm ein Kompliment.",
			"test-f": "Susanne bewundert seine Brille und macht ihm ein Kompliment.",
			"test-n": "Susanne bewundert seine Brille und macht ihm ein Kompliment."
		},
		{
			"trigger": "auch",
			"verifying": "Karl hat ein Geschenk für " + exp.names['auch'][0]['v_name'] + " mitgebracht.",
			"falsifying": "Niemand hat ein Geschenk für " + exp.names['auch'][0]['f_name'] + " mitgebracht.",
			"neutral": "Karl hat keinen Salat für " + exp.names['auch'][0]['n_name'] + " mitgebracht.",
			"test-v": exp.names['auch'][0]['v_name'] + " vermutet, dass auch Fritz ein Geschenk für sie mitgebracht hat und ist deswegen fröhlich.",
			"test-f": exp.names['auch'][0]['f_name'] + " vermutet, dass auch Fritz ein Geschenk für sie mitgebracht hat und ist deswegen fröhlich.",
			"test-n": exp.names['auch'][0]['n_name'] + " vermutet, dass auch Fritz ein Geschenk für sie mitgebracht hat und ist deswegen fröhlich."
		},
		{
			"trigger": "auch",
			"verifying": "Fritz kocht heute eine Suppe mit " + exp.names['auch'][1]['v_name'] + ".",
			"falsifying": "Niemand kocht heute eine Suppe mit " + exp.names['auch'][1]['f_name'] + ".",
			"neutral": "Niemand isst heute ein Eis mit " + exp.names['auch'][1]['n_name'] + ".",
			"test-v": exp.names['auch'][1]['v_name'] + " hofft, dass auch Susanne heute eine Suppe mit ihr kocht und kauft dafür Zutaten.",
			"test-f": exp.names['auch'][1]['f_name'] + " hofft, dass auch Susanne heute eine Suppe mit ihr kocht und kauft dafür Zutaten.",
			"test-n": exp.names['auch'][1]['n_name'] + " hofft, dass auch Susanne heute eine Suppe mit ihr kocht und kauft dafür Zutaten."
		},
		{
			"trigger": "auch",
			"verifying": "Karl schuldet " + exp.names['auch'][2]['v_name']+ " noch Geld.",
			"falsifying": "Niemand schuldet " + exp.names['auch'][2]['f_name'] + " noch Geld.",
			"neutral": "Karl schuldet " + exp.names['auch'][2]['n_name'] + " noch einen Gefallen.",
			"test-v": exp.names['auch'][2]['v_name'] + " glaubt, dass auch Inge ihr noch Geld schuldet und ruft sie an.",
			"test-f": exp.names['auch'][2]['f_name'] + " glaubt, dass auch Inge ihr noch Geld schuldet und ruft sie an.",
			"test-n": exp.names['auch'][2]['n_name'] + " glaubt, dass auch Inge ihr noch Geld schuldet und ruft sie an."
		},
		{
			"trigger": "auch",
			"verifying": "Fritz schreibt einen Song für " + exp.names['auch'][3]['v_name'] + ".",
			"falsifying": "Niemand schreibt einen Song für " + exp.names['auch'][3]['f_name'] + ".",
			"neutral": "Niemand schreibt ein Gedicht für " +  exp.names['auch'][3]['n_name'] + ".",
			"test-v": exp.names['auch'][3]['v_name'] + " hofft, dass auch Karl einen Song für sie schreibt und ist gespannt.",
			"test-f": exp.names['auch'][3]['f_name'] + " hofft, dass auch Karl einen Song für sie schreibt und ist gespannt.",
			"test-n": exp.names['auch'][3]['n_name'] + " hofft, dass auch Karl einen Song für sie schreibt und ist gespannt."
		},
		{
			"trigger": "auch",
			"verifying": "Heute machen alle " + exp.names['auch'][4]['v_name'] + " ein Kompliment.",
			"falsifying": "Heute macht niemand " + exp.names['auch'][4]['f_name'] + " ein Kompliment.",
			"neutral": "Heute macht Fritz " + exp.names['auch'][4]['n_name'] + " einen Heiratsantrag.",
			"test-v": exp.names['auch'][4]['v_name'] + " nimmt an, dass auch Karl ihr heute ein Kompliment machen wird und wartet ungeduldig.",
			"test-f": exp.names['auch'][4]['f_name'] + " nimmt an, dass auch Karl ihr heute ein Kompliment machen wird und wartet ungeduldig.",
			"test-n": exp.names['auch'][4]['n_name'] + " nimmt an, dass auch Karl ihr heute ein Kompliment machen wird und wartet ungeduldig."
		},
		{
			"trigger": "auch",
			"verifying": "Fritz schenkt " + exp.names['auch'][5]['v_name'] + " ein Kochbuch zu Weihnachten.",
			"falsifying": "Alle schenken " + exp.names['auch'][5]['f_name'] + " Kochbuch zu Weihnachten.",
			"neutral": "Alle schenken " + exp.names['auch'][5]['n_name'] + " Plätzchen zu Weihnachten.",
			"test-v": exp.names['auch'][5]['v_name'] + " fürchtet, dass auch Tina ihr ein Kochbuch zu Weihnachten schenken wird und ist betrübt.",
			"test-f": exp.names['auch'][5]['f_name'] + " fürchtet, dass auch Tina ihr ein Kochbuch zu Weihnachten schenken wird und ist betrübt.",
			"test-n": exp.names['auch'][5]['n_name'] + " fürchtet, dass auch Tina ihr ein Kochbuch zu Weihnachten schenken wird und ist betrübt."
		},
		{
			"trigger": "auch",
			"verifying": "Fritz kocht am Wochenende für " + exp.names['auch'][6]['v_name'] + ".",
			"falsifying": "Niemand kocht am Wochenende für " + exp.names['auch'][6]['f_name'] + ".",
			"neutral": "Fritz singt am Wochenende für " + exp.names['auch'][6]['n_name'] + ".",
			"test-v": exp.names['auch'][6]['v_name'] + " wünscht sich, dass auch Peter am Wochenende für sie kocht und sagt ihm das.",
			"test-f": exp.names['auch'][6]['f_name'] + " wünscht sich, dass auch Peter am Wochenende für sie kocht und sagt ihm das.",
			"test-n": exp.names['auch'][6]['n_name'] + " wünscht sich, dass auch Peter am Wochenende für sie kocht und sagt ihm das."
		},
		{
			"trigger": "auch",
			"verifying": "Niemand sammelt Pilze für " + exp.names['auch'][7]['v_name'] + " .",
			"falsifying": "Karl sammelt Pilze für " + exp.names['auch'][7]['f_name'] + ".",
			"neutral": exp.names['auch'][7]['v_name'] + " sammelt Briefmarken für Karl.",
			"test-v": exp.names['auch'][7]['v_name'] + " vermutet, dass auch Anton keine Pilze für sie sammelt und geht selbst welche suchen.",
			"test-f": exp.names['auch'][7]['f_name'] + " vermutet, dass auch Anton keine Pilze für sie sammelt und geht selbst welche suchen.",
			"test-n": exp.names['auch'][7]['n_name'] + " vermutet, dass auch Anton keine Pilze für sie sammelt und geht selbst welche suchen."
		},
		{
			"trigger": "auch",
			"verifying": "Karl findet " + exp.names['auch'][8]['v_name'] + " unsympathisch.",
			"falsifying": "Alle finden " + exp.names['auch'][8]['f_name'] + " sympathisch.",
			"neutral": "Karl findet " + exp.names['auch'][8]['n_name'] + " gutaussehend.",
			"test-v": "Tina denkt, dass auch Fritz " + exp.names['auch'][8]['v_name'] + " unsympathisch findet und kann das nicht verstehen.",
			"test-f": "Tina denkt, dass auch Fritz " + exp.names['auch'][8]['f_name'] + " unsympathisch findet und kann das nicht verstehen.",
			"test-n": "Tina denkt, dass auch Fritz " + exp.names['auch'][8]['n_name'] + " unsympathisch findet und kann das nicht verstehen."
		},
		{
			"trigger": "auch",
			"verifying": "Karl findet " + exp.names['auch'][9]['v_name']  + "s neue Frisur schrecklich.",
			"falsifying": "Alle finden " + exp.names['auch'][9]['f_name']  + "s neue Frisur gut.",
			"neutral": "Alle finden " + exp.names['auch'][9]['n_name']  + "s neue Tasche gut.",
			"test-v": exp.names['auch'][9]['v_name'] + " befürchtet, dass auch Fritz ihre neue Frisur schrecklich findet und versteckt sich.",
			"test-f": exp.names['auch'][9]['f_name'] + " befürchtet, dass auch Fritz ihre neue Frisur schrecklich findet und versteckt sich.",
			"test-n": exp.names['auch'][9]['n_name'] + " befürchtet, dass auch Fritz ihre neue Frisur schrecklich findet und versteckt sich."
		},
		{
			"trigger": "auch",
			"verifying": "Anton hat heute eine schlechte Nachricht für " + exp.names['auch'][10]['v_name'] + ".",
			"falsifying": "Niemand hat heute eine schlechte Nachricht für " + exp.names['auch'][10]['f_name'] + ".",
			"neutral": "Peter hat heute eine interessante Nachricht für " + exp.names['auch'][10]['n_name'] + ".",
			"test-v": exp.names['auch'][10]['v_name'] + " ahnt, dass auch Susanne heute eine schlechte Nachricht für sie hat und meidet sie.",
			"test-f": exp.names['auch'][10]['f_name'] + " ahnt, dass auch Susanne heute eine schlechte Nachricht für sie hat und meidet sie.",
			"test-n": exp.names['auch'][10]['n_name'] + " ahnt, dass auch Susanne heute eine schlechte Nachricht für sie hat und meidet sie."
		},
		{
			"trigger": "auch",
			"verifying": "Fritz kauft eine Kaffeemaschine für " + exp.names['auch'][11]['v_name'] + ".",
			"falsifying": "Niemand kauft eine Kaffeemaschine für "+ exp.names['auch'][11]['f_name'] +".",
			"neutral": " Niemand kauft einen Wasserkocher für "+ exp.names['auch'][11]['n_name'] + ".",
			"test-v": exp.names['auch'][11]['v_name'] + " geht davon aus, dass auch Tina eine Kaffeemaschine für sie kauft und will sie davon abhalten.",
			"test-f": exp.names['auch'][11]['f_name'] + " geht davon aus, dass auch Tina eine Kaffeemaschine für sie kauft und will sie davon abhalten.",
			"test-n": exp.names['auch'][11]['n_name'] + " geht davon aus, dass auch Tina eine Kaffeemaschine für sie kauft und will sie davon abhalten."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][0]['v_name'] + " hilft in einem Altersheim aus.",
			"falsifying": exp.names['aufhören'][0]['f_name'] + " hilft nicht in einem Altersheim aus.",
			"neutral": " Fritz hilft in einem Altersheim aus.",
			"test-v": exp.names['aufhören'][0]['v_name'] + " wird aufhören im Altersheim auszuhelfen und teilt dies einem Vorgesetzten mit.",
			"test-f": exp.names['aufhören'][0]['f_name'] + " wird aufhören im Altersheim auszuhelfen und teilt dies einem Vorgesetzten mit.",
			"test-n": exp.names['aufhören'][0]['n_name'] + " wird aufhören im Altersheim auszuhelfen und teilt dies einem Vorgesetzten mit."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][1]['v_name'] + " geht abends oft joggen.",
			"falsifying": exp.names['aufhören'][1]['f_name'] + " geht abends nie joggen.",
			"neutral": "Karl geht abends nie joggen.",
			"test-v": exp.names['aufhören'][1]['v_name'] + " wird aufhören abends joggen zu gehen und meldet sich zum Yoga an.",
			"test-f": exp.names['aufhören'][1]['f_name'] + " wird aufhören abends joggen zu gehen und meldet sich zum Yoga an.",
			"test-n": exp.names['aufhören'][1]['n_name'] + " wird aufhören abends joggen zu gehen und meldet sich zum Yoga an."
		},
		{
			"trigger": "aufhören",
			"verifying": "Susanne und " + exp.names['aufhören'][11]['v_name'] + " gehen oft gemeinsam Tanzen.",
			"falsifying": "Susanne und " + exp.names['aufhören'][11]['f_name'] + " gehen nie gemeinsam Tanzen.",
			"neutral": "Tina und Karl gehen oft gemeinsam Tanzen.",
			"test-v": "Susanne und " + exp.names['aufhören'][11]['v_name'] + " werden aufhören gemeinsam Tanzen zu gehen, weil Fritz Knieprobleme hat.",
			"test-f": "Susanne und " + exp.names['aufhören'][11]['f_name'] + " werden aufhören gemeinsam Tanzen zu gehen, weil Fritz Knieprobleme hat.",
			"test-n": "Susanne und " + exp.names['aufhören'][11]['n_name'] + " werden aufhören gemeinsam Tanzen zu gehen, weil Fritz Knieprobleme hat."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][2]['v_name'] + " sammelt Briefmarken.",
			"falsifying": exp.names['aufhören'][2]['f_name'] + " sammelt keine Briefmarken.",
			"neutral": "Karl sammelt keine Briefmarken.",
			"test-v": exp.names['aufhören'][2]['v_name'] + " wird aufhören Briefmarken zu sammeln, weil es zu teuer ist.",
			"test-f": exp.names['aufhören'][2]['f_name'] + " wird aufhören Briefmarken zu sammeln, weil es zu teuer ist.",
			"test-n": exp.names['aufhören'][2]['n_name'] + " wird aufhören Briefmarken zu sammeln, weil es zu teuer ist."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][3]['v_name'] + " hat einen alten Corsa, den er nie abschließt.",
			"falsifying": exp.names['aufhören'][3]['f_name'] + " hat einen alten Corsa, den er immer abschließt.",
			"neutral": "Anselm hat einen alten Corsa, den er nie abschließt.",
			"test-v": exp.names['aufhören'][3]['v_name'] + " wird aufhören seinen Corsa offen stehen zu lassen, weil er Angst vor Dieben hat.",
			"test-f": exp.names['aufhören'][3]['f_name'] + " wird aufhören seinen Corsa offen stehen zu lassen, weil er Angst vor Dieben hat.",
			"test-n": exp.names['aufhören'][3]['n_name'] + " wird aufhören seinen Corsa offen stehen zu lassen, weil er Angst vor Dieben hat."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][4]['v_name'] + " ist Raucher.",
			"falsifying": exp.names['aufhören'][4]['f_name'] + " ist Nichtraucher.",
			"neutral": "Moritz ist Nichtraucher.",
			"test-v": exp.names['aufhören'][4]['v_name'] + " wird aufhören zu rauchen, weil es ungesund ist.",
			"test-f": exp.names['aufhören'][4]['f_name'] + " wird aufhören zu rauchen, weil es ungesund ist.",
			"test-n": exp.names['aufhören'][4]['n_name'] + " wird aufhören zu rauchen, weil es ungesund ist."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][5]['v_name'] + " isst kein Fleisch.",
			"falsifying": exp.names['aufhören'][5]['f_name'] + " isst täglich Fleisch.",
			"neutral": "Dominic isst kein Fleisch.",
			"test-v": exp.names['aufhören'][5]['v_name'] + " wird aufhören sich vegetarisch zu ernähren, weil er Eisenmangel hat.",
			"test-f": exp.names['aufhören'][5]['f_name'] + " wird aufhören sich vegetarisch zu ernähren, weil er Eisenmangel hat.",
			"test-n": exp.names['aufhören'][5]['n_name'] + " wird aufhören sich vegetarisch zu ernähren, weil er Eisenmangel hat."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][6]['v_name'] + " geht oft auf Partys.",
			"falsifying": exp.names['aufhören'][6]['f_name'] + " geht nie auf Partys.",
			"neutral": "Karl geht oft auf Partys.",
			"test-v": exp.names['aufhören'][6]['v_name'] + " wird aufhören auf Partys zu gehen, weil er mehr lernen muss.",
			"test-f": exp.names['aufhören'][6]['f_name'] + " wird aufhören auf Partys zu gehen, weil er mehr lernen muss.",
			"test-n": exp.names['aufhören'][6]['n_name'] + " wird aufhören auf Partys zu gehen, weil er mehr lernen muss."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][7]['v_name'] + " macht zurzeit eine Diät.",
			"falsifying": exp.names['aufhören'][7]['f_name'] + " macht zurzeit keine Diät.",
			"neutral": "Anselm macht zurzeit eine Diät.",
			"test-v": exp.names['aufhören'][7]['v_name'] + " wird aufhören eine Diät zu machen, weil er Süßes zu sehr vermisst.",
			"test-f": exp.names['aufhören'][7]['f_name'] + " wird aufhören eine Diät zu machen, weil er Süßes zu sehr vermisst.",
			"test-n": exp.names['aufhören'][7]['n_name'] + " wird aufhören eine Diät zu machen, weil er Süßes zu sehr vermisst."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][8]['v_name'] + " macht täglich Sport.",
			"falsifying": exp.names['aufhören'][8]['f_name'] + " macht nie Sport.",
			"neutral": "Moritz macht täglich Sport.",
			"test-v": exp.names['aufhören'][8]['v_name'] + " wird aufhören täglich Sport zu machen, weil er Knieprobleme hat.",
			"test-f": exp.names['aufhören'][8]['f_name'] + " wird aufhören täglich Sport zu machen, weil er Knieprobleme hat.",
			"test-n": exp.names['aufhören'][8]['n_name'] + " wird aufhören täglich Sport zu machen, weil er Knieprobleme hat."
		},
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][9]['v_name'] + " isst nie Fastfood.",
			"falsifying": exp.names['aufhören'][9]['f_name'] + " isst nur Fastfood.",
			"neutral": "Jan isst nie Fastfood.",
			"test-v": exp.names['aufhören'][9]['v_name'] + " wird ab jetzt aufhören Fastfood zu essen, weil dick geworden ist.",
			"test-f": exp.names['aufhören'][9]['f_name'] + " wird ab jetzt aufhören Fastfood zu essen, weil dick geworden ist.",
			"test-n": exp.names['aufhören'][9]['n_name'] + " wird ab jetzt aufhören Fastfood zu essen, weil dick geworden ist."
		},    	    
		{
			"trigger": "aufhören",
			"verifying": exp.names['aufhören'][10]['v_name'] + " spendet nie Geld für soziale Zwecke.",
			"falsifying": exp.names['aufhören'][10]['f_name'] + " spendet immer Geld für soziale Zwecke.",
			"neutral": "Hans spendet oft Geld für soziale Zwecke.",
			"test-v": exp.names['aufhören'][10]['v_name'] + " wird aufhören Spendenaufrufe zu ignorieren, weil er ein schlechtes Gewissen hat.",
			"test-f": exp.names['aufhören'][10]['f_name'] + " wird aufhören Spendenaufrufe zu ignorieren, weil er ein schlechtes Gewissen hat.",
			"test-n": exp.names['aufhören'][10]['n_name'] + " wird aufhören Spendenaufrufe zu ignorieren, weil er ein schlechtes Gewissen hat."
		}
	];

	for (var i = 0; i < items.length; i++) {
		groups[Math.floor(i / 12)].push(items[i]);
	}

	for (var i = 0; i < groups.length; i++) {
		for (var j = 0; j < groups[i].length; j++) {
			lists[Math.floor(j % 6)].push(groups[i][j]);
		}
	}

	console.log('6 lists created');
	console.log(lists);
	console.log("ITEMS LOADED");

	return lists[Math.floor(Math.random() * lists.length)];
};