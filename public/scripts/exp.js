var prepareNames = function() {
	var trials_meta = {'wieder': 12, 'wissen': 12, 'aufhören': 12, 'sein': 12, 'auch': 12};
	var names = {};

	// Type_1: (wissen, wieder, aufhören) Ver_male_var_j, Fals_male_var_k, Test_male_var_j/k/n (neutral male name kept constant)
	// Type_2: (sein) Ver_male_var_j, Fals_male_var_k, Neutr_male_var_n (no name variable in test-sentence)
	// Type_3: (auch) Ver_female_var_j, Fals_female_var_k, Neutr_female_var_n, Test_female_var_j/k/n
	var pickNames = function() {
		var pickedNames = {};

		for (trigger in trials_meta) {
			// Type 1 :: male names
			if (trials_meta.hasOwnProperty(trigger) && (trigger === 'wieder') || (trigger === 'wissen') || (trigger === 'aufhören')) {
				for (var i = 0; i < trials_meta[trigger]; i++) {
					var names = {};
					var gmn = ["Peter", "Anton", "Stefan", "Patrick", "Nico", "David", "Uwe", "Martin", "Simon", "Holger", "Jakob", "Luca", "Marcel", "Norbert", "Olaf", "Pascal", "Robin", "Werner", "Otto", "Rudolf", "Thorsten"];
					names["v_name"] = gmn[Math.floor(Math.random()*gmn.length)];
					gmn.splice(gmn.indexOf(names[0]), 1);
					names["f_name"] = gmn[Math.floor(Math.random()*gmn.length)];
					gmn.splice(gmn.indexOf(names[1]), 1);
					names["n_name"] = gmn[Math.floor(Math.random()*gmn.length)];

					if (pickedNames[trigger] === undefined) {
						pickedNames[trigger] = [];
						pickedNames[trigger].push(names);
					} else {
						pickedNames[trigger].push(names);
					}
				}
			}
			// Type 2 :: male names
			else if (trials_meta.hasOwnProperty(trigger) && trigger === 'sein') {
				for (var i = 0; i < trials_meta[trigger]; i++) {
					var names = {};
					var gmn = ["Peter", "Anton", "Stefan", "Patrick", "Nico", "David", "Uwe", "Martin", "Simon", "Holger", "Jakob", "Luca", "Marcel", "Norbert", "Olaf", "Pascal", "Robin", "Werner", "Otto", "Rudolf", "Thorsten"];
					names["v_name"] = gmn[Math.floor(Math.random()*gmn.length)];
					gmn.splice(gmn.indexOf(names["v_name"]), 1);
					names["f_name"] = gmn[Math.floor(Math.random()*gmn.length)];
					gmn.splice(gmn.indexOf(names["f_name"]), 1);
					names["n_name"] = gmn[Math.floor(Math.random()*gmn.length)];

					if (pickedNames[trigger] === undefined) {
						pickedNames[trigger] = [];
						pickedNames[trigger].push(names);
					} else {
						pickedNames[trigger].push(names);
					}
				}
			}
			// Type 3 :: female names
			else if (trials_meta.hasOwnProperty(trigger) && trigger === 'auch') {
				for (var i = 0; i < trials_meta[trigger]; i++) {
					var names = {};
					var gfn = ["Anna", "Anke", "Lina", "Inge", "Tina", "Vera", "Saskia", "Sigrid", "Edith", "Esther", "Gertrud", "Jana", "Judith", "Karen", "Linda", "Lydia", "Mona", "Olga", "Petra", "Britta", "Rose", "Astrid", "Berta", "Carla", "Dana", "Dorit", "Eva", "Ingrid"];
					names["v_name"] = gfn[Math.floor(Math.random()*gfn.length)];
					gfn.splice(gfn.indexOf(names["v_name"]), 1);
					names["f_name"] = gfn[Math.floor(Math.random()*gfn.length)];
					gfn.splice(gfn.indexOf(names["f_name"]), 1);
					names["n_name"] = gfn[Math.floor(Math.random()*gfn.length)];

					if (pickedNames[trigger] === undefined) {
						pickedNames[trigger] = [];
						pickedNames[trigger].push(names);
					} else {
						pickedNames[trigger].push(names);
					}
				}
			} else {
				console.log('no such trigger');
			}
		}

		return pickedNames;
	};

	names = pickNames();

	return names;
};

var initExp = function(items) {
	var data = {};
	var preparedItems;

	// items in trial format
	var prepareItems = function(items) {
		var trials = [];

		for (var i = 0; i < items.length; i++) {
			var v = {'type': 'verifying', 'context': items[i]['verifying'], 'sentence': items[i]['test-v'], trigger: items[i]['trigger']};
			var f = {'type': 'falsifying', 'context': items[i]['falsifying'], 'sentence': items[i]['test-f'], trigger: items[i]['trigger']};
			var n = {'type': 'neutral', 'context': items[i]['neutral'], 'sentence': items[i]['test-n'], trigger: items[i]['trigger']};
			trials.push(v, f, n);
		}

		return trials;
	};

	// shuffles the items and verifies two items of the same trigger
	var shuffleItems = function(items) {
		console.log('shuffling items');
		var trials = [];
		var okay, index, i;

		while (items.length > 0) {
			okay = false;
			for (i = 0; i < 4; i++) {
				index = Math.floor(Math.random() * items.length);

				if ((trials.length === 0) || (trials[trials.length - 1]['trigger'] !== items[index]['trigger'])) {
					trials.push(items[index]);
					items.splice(index, 1);
					okay = true;
					break;
				}
			}

			if (!okay) {
				for (var i = 0; i < trials.length; i++) {
					items.push(trials[i]);
				}

				throw "possible infinite loop";
			}
		}

		return trials;
	};

	preparedItems = prepareItems(items);

	while (true) {
		try {
			data.trials = shuffleItems(preparedItems);
			break;
		} catch (e) {
			console.debug(e);
		}
	}

	data.practice_trials = [
		{
			context: "Rudolf hat letzte Woche Pinguine gefüttert.",
			sentence: "Heute hat Rudolf wieder Pinguine gefüttert und freut sich.",
			trigger: "wieder",
			type: "verifying"
		},
		{
			context: "Holger besitzt kein Restaurant.",
			sentence: "Susanne geht in sein Restaurant und trifft Bekannte.",
			trigger: "sein",
			type: "falsifying"
		}
	];
	data.out = [];

	return data;
};