// creates Trial View
var initTrialView = function(index) {
    var view = {};
    view.name = 'trial';
    view.template = $('#trial-view').html();
    var trialInfo = exp.data.trials[index];
    var filled = index * (180 / config_general.viewSteps[exp.currentViewCounter]);
    var readingDates = [];
    var readingTimes = [];
    var rtCount = exp.data.trials[index].sentence.split(" ").length;
    var sentence = initSentence();
    var startingTime = Date.now();
    var space = 0;

    // renders the templ
    $('#main').html(Mustache.render(view.template, {
        currentTrial: index + 1,
        totalTrials: exp.data.trials.length,
        sentence: trialInfo.sentence.split(" "),
        context: trialInfo.context,
        spaceInstructions: config_general.expSettings.spaceInstructions,
        helpText: config_general.expSettings.helpText
    }));

    $('#filled').css('width', filled);

    // creates one continuous underline below the sentence if it was set to true in config.js
    if (config_general.expSettings.underlineOneLine === true) {
        var words = $(".word");

        for (var i=0; i<words.length; i++) {
            $(words[i]).css('margin', '0 -3px');
        }
    }

    // hides the fixation point and shows the stimulus
    var showStimulus = function() {
        $('.stimulus').removeClass('nodisplay');
        $('.cross-container').addClass('nodisplay');
        $('body').on('keyup', handleKeyUp);
    };

    var hideStimulus = function() {
        $('.stimulus').removeClass('nodisplay');
        $('.cross-container').addClass('nodisplay');
    };

    // shows the QUD for a second and then the fixation cross appears
    // calls showStimulus after a 'pause' amount of time
    setTimeout(function() {
        $('.cross-container').removeClass('nodisplay');
        setTimeout(showStimulus, config_general.expSettings.crossDuration);
    }, config_general.expSettings.pause);

    // checks whether the key pressed is space and if so calls sentence.showNextWord()
    // handleKeyUp() is called when a key is pressed
    var handleKeyUp = function(e) {
        if (e.which === 32) {
            space++;

            if (space > 1) {
                $('.help-text').addClass('hidden');
                sentence.showNextWord();

                // collects the dates (unix time) in a variable readingDates every time a word is shown
                if (rtCount >= 0) {
                    readingDates.push(Date.now());
                }
                rtCount--;
            } else {
                $('.help-text').removeClass('hidden');
                $('.space-instructions').addClass('hidden');
                $('.sentence').removeClass('nodisplay');
                $('.img').addClass('nodisplay');
            }
        }
    };

    // converts the readingDates into readingTimes by substracting
    // returns a list of readingTimes
    var getDeltas = function() {
        var deltas = [];

        for (var i = 0; i < readingDates.length - 1; i++) {
            deltas[i] = readingDates[i+1] - readingDates[i];
        };

        return deltas;
    };

    // attaches an event listener to the yes / no radio inputs
    // when an input is selected a response property with a value equal to the answer is added to the trial object
    // as well as a readingTimes property with value - a list containing the reading times of each word
    $('input[name=question]').on('change', function() {
        $('body').off('keyup', handleKeyUp);
        $('#next').removeClass('nodisplay');
        var trialData = {};

        for (var prop in trialInfo) {
            if (trialInfo.hasOwnProperty(prop)) {
                trialData[prop] = trialInfo[prop];
            }
        }

        trialData.time_spent = Date.now() - startingTime;
        trialData.trial_number = index + 1;
        trialData.response = $('input[name=question]:checked').val();
        trialData.reading_times = getDeltas();
        exp.data.out.push(trialData);
    });

    $('#next').on('click', function() {
        exp.findNextView();
    });

    return view;
};