// creates Practice view
var initPracticeView = function(CT) {
    var view = {};
    view.name = 'practice',
    view.template = $('#practice-view').html();
    var space = 0;
    var sentence = initSentence();
    var trialInfo = exp.data.practice_trials[CT];

    $('#main').html(Mustache.render(view.template, {
        title: config_views.practice.title,
        sentence: trialInfo.sentence.split(" "),
        context: trialInfo.context,
        spaceInstructions: config_general.expSettings.spaceInstructions,
        helpText: config_general.expSettings.helpText
    }));

    // creates one continuous underline below the sentence if it was set to true in config.js
    if (config_general.expSettings.underlineOneLine === true) {
        var words = $(".word");

        for (var i=0; i<words.length; i++) {
            $(words[i]).css('margin', '0 -3px');
        }
    }

    // hides the fixation point and shows the stimulus
    var showStimulus = function() {
        $('.stimulus').removeClass('nodisplay');
        $('.cross-container').addClass('nodisplay');
        $('body').on('keyup', handleKeyUp);
    };

    // blank screen
    setTimeout(function() {
        $('.cross-container').removeClass('nodisplay');
        setTimeout(showStimulus, config_general.expSettings.crossDuration);
    }, config_general.expSettings.pause);


    // checks whether the key pressed is space and if so calls sentence.showNextWord()
    // handleKeyUp() is called when a key is pressed
    var handleKeyUp = function(e) {
        space++;

        if (e.which === 32) {
            if (space > 1) {
                $('.help-text').addClass('hidden');
                sentence.showNextWord();
            } else {
                $('.help-text').removeClass('hidden');
                $('.space-instructions').addClass('hidden');
                $('.sentence').removeClass('nodisplay');
                $('.img').addClass('nodisplay');
            }
        }
    };

    $('input[name=question]').on('change', function() {
        $('body').off('keyup', handleKeyUp);
        $('#next').removeClass('nodisplay');
    });

    $('#next').on('click', function() {
        exp.findNextView();
    });

    return view;
};