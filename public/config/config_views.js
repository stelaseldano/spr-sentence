// can be left empty string if there is no need of it
var config_views = {
	// intro view
	"intro": {
		// introduction title
		"title": "Welcome to XPrag experiments!",
		// text that asks for ID (used when the config_deploy.deployMethod is Prolific, directLink)
		"IDtext": "Please enter your Prolific ID:",
		// introduction text
		"text": "Lorem ipsum dolor sit amet consectetur adipiscing elit tellus auctor, risus metus mauris nibh leo senectus varius taciti bibendum, laoreet tempor orci ligula iaculis odio malesuada nostra. Erat gravida consequat nunc pharetra libero tempus lobortis placerat, laoreet vitae eget vivamus eros luctus sed nullam auctor, vel sem hac quam facilisis aptent blandit. Bibendum molestie morbi ullamcorper vitae accumsan dapibus ultricies aliquet mi luctus, ante suscipit purus consequat nascetur a senectus cras donec.",
		// instroduction's slide proceeding button
		"buttonText": "Start"
	},

	// instructions view
	"instructions": {
		// instruction's title
		"title": "Instructions",
		// instruction's text
		"text": "instructions on how to do the exp",
		// instuction's slide proceeding button text
		"buttonText": "Go to practice trial"
	},

	// practice trial view
	"practice": {
		"title": "Practice Trial"
	},

	// begin experiment view
	"beginExp": {
		"text": "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus."
	},

	// subject info view
	"postTest": {
		"title": "Additional Information",
		"text": "Answering the following questions is optional, but will help us understand your answers.",
		"buttonText": "Continue",
	},

	// thanks view
	"thanks": {
		"message": "Thank you for taking part in this experiment!"
	}
};